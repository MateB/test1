package tests;
import utilities.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MatrixMethodTests {

    int[][] x = {{1,2,3}, {4,5,6}};
    int[][] testArr = {{1,1,2,2,3,3},{4,4,5,5,6,6}};

    @Test
    void duplicateTest()
    {
        int[][] y = MatrixMethod.duplicate(x);
        assertArrayEquals(testArr , y);
    }


}
