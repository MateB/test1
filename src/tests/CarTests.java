package tests;
import automobiles.*;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CarTests {
	
	//Testing the throw illegal exception
	@Test
	void throwTest()
	{
		try 
		{
			Car c1 = new Car(-10);
			fail("Car constructor should throw an exception because of negative speed");
		}
		catch (IllegalArgumentException e)
		{
			//it ran well
		}
		catch (Exception e)
		{
			fail("Car constructor threw the wrong exception type");
		}
	}
	//Testing getSpeed method
	@Test
	void getSpeedTest() 
	{
		Car c1 = new Car(10);
		assertEquals(10, c1.getSpeed());
	}
	//Testing getLocation method
	@Test
	void getLocationTest()
	{
		Car c1 = new Car(10);
		assertEquals(50, c1.getLocation());
	}
	//Testing max_position value
	@Test
	void MAX_POSITION_TEST()
	{
		Car c1 = new Car(10);
		assertEquals(100, c1.getLocation()*2 );
	}
	//Testing moveRight method with big speed
	//named it with Max because of input of speed
	@Test
	void moveRightTestMax()
	{
		Car c1 = new Car(70);
		c1.moveRight();
		assertEquals(100, c1.getLocation());
	}
	//Testing moveRight method with small speed
	//named it min since its a small speed input
	@Test
	void moveRightTestMin()
	{
		Car c1 = new Car(10);
		c1.moveRight();
		assertEquals(60, c1.getLocation());
	}
	/*Testing moveLeft method big speed
	this test should lead to having a speed under 0
	named it with Max because of the input of speed*/
	@Test
	void moveLeftTestMax()
	{
		Car c1 = new Car(60);
		c1.moveLeft();
		assertEquals(0, c1.getLocation());
	}
	/*Testing moveLeft method small speed
	this test should lead to having 1\
	named it with Min because of the input of speed*/
	@Test
	void moveLeftTestMin()
	{
		Car c1 = new Car(49);
		c1.moveLeft();
		assertEquals(1, c1.getLocation());
	}
	//Testing accelerate method
	@Test
	void accelerateTest()
	{
		Car c1 = new Car(10);
		c1.accelerate();
		assertEquals(11, c1.getSpeed());
	}
	//Testing stop method
	@Test
	void stopTest()
	{
		Car c1 = new Car(10);
		c1.stop();
		assertEquals(0, c1.getSpeed());
	}

}
